from flask import Flask,redirect,session,request,abort,render_template
from requests import Request,post
import requests
import uuid
from wtforms import Form, StringField, FileField, SubmitField, validators
import client

app = Flask(__name__)
app.debug = False


class Resume(Form):
    first_name = StringField('First Name')
    last_name = StringField('Last Name')
    projects_url = StringField('Projects Url')
    code_url = StringField('Code Url')
    resume = FileField('Resume')
    submit = SubmitField()


@app.route('/')
def start():
    state = uuid.uuid4().get_hex()
    session['agiliq_auth_state']= state
    params = {
        'client_id' : client.client_id,
        'state' : state,
        'redirect_uri' : client.redirect_uri
    }
    agiliq_auth_url = 'http://join.agiliq.com/oauth/authorize/'
    r = Request('GET',url=agiliq_auth_url,params=params).prepare()
    return redirect(r.url)


@app.route('/callback',methods=['GET','POST'])
def acess_token():
    callback_state = session.get('agiliq_auth_state')
    if not original_state:
        abort(404)
    
    state = request.args.get('state')
    code = request.args.get('code')

    if not state or not code:
        abort(404)
    if callback_state!=state:
        abort(404)
    params = {
        'client_id' : client.client_id,
        'client_secret' : client.client_secret,
        'code' : code,
        'redirect_uri': client.redirect_uri,
    }
    headers = {'accept':'application/json'}
    url = 'http://join.agiliq.com/oauth/access_token/'
    r = post(url,params=params,headers=headers)
    
    if not r.ok:
        abort(404)
    
    data = r.json()
    access_token = data['access_token']
    form = Resume()
    return render_template('resume.html',form=form,access_token=access_token)


if __name__ == '__main__':
    app.run()
